var http = require("http"); 
 
http.createServer(function (request, response) { 
 
   // Send the HTTP header  
   // HTTP Status 200 OK 
   // Content Type is text/plain 
   response.writeHead(200, {'Content-Type': 'text/plain'}); 
    
   // Send response body as "Hello World" 
   response.end('Hello Git\n'); 
}).listen(8080); 
 
// Print message 
console.log('Server running at http://127.0.0.1:8080/'); 
